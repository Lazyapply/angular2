///<reference path="../../node_modules/@angular/forms/src/form_providers.d.ts"/>
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FavoriteComponent } from './components/shared/favorites/favorites.component';
import { PanelComponent } from './components/shared/panel/panel.component';
import { LikeComponent } from './components/shared/like/like.component';
import { NgSwitchComponent } from './components/ng-switch/ng-switch.component';
import { InputFormatDirective } from './input-format.directive';
import { ZippyComponent } from './components/shared/zippy/zippy.component';
import { ContactFormComponent } from './components/shared/contact-form/contact-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { NewCourseComponentComponent } from './components/shared/new-course-component/new-course-component.component';
import { NewCourseComponentWithFormBuilderComponent } from './components/shared/new-course-component-with-form-builder/new-course-component-with-form-builder.component';
import { PostsComponentComponent } from './components/shared/posts-component/posts-component.component';
import { HttpModule } from '@angular/http';
import { PostService } from './services/post.service';
import { AppErrorHandler } from './components/shared/ErrorHandlers/ErrorHandler';


@NgModule({
  declarations: [
    AppComponent,
    SignupFormComponent,
    FavoriteComponent,
    PanelComponent,
    LikeComponent,
    NgSwitchComponent,
    InputFormatDirective,
    ZippyComponent,
    ContactFormComponent,
    NewCourseComponentComponent,
    NewCourseComponentWithFormBuilderComponent,
    PostsComponentComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [
    PostService,
    { provide: ErrorHandler, useClass: AppErrorHandler },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
