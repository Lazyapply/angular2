import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { AppError } from '../components/shared/ErrorHandlers/AppError';
import 'rxjs/add/observable/throw';
import { NotFoundError } from '../components/shared/ErrorHandlers/NotFoundError';
import { BadRequestError } from '../components/shared/ErrorHandlers/BadRequestError';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  constructor(private url: string, private http: Http) {}

  getAll() {
    return this.http.get(this.url)
      .map(response => response.json())
      .catch(this.handleErrorResponse);
  }

  create(resource) {
    return this.http.post(this.url, JSON.stringify(resource))
      .map(response => response.json())
      .catch(this.handleErrorResponse);
  }

  update(resource) {
    return this.http.patch(`${this.url}/${resource.id}`, JSON.stringify({ isRead: true }))
      .map(response => response.json())
      .catch(this.handleErrorResponse);
  }

  delete(id) {
    return this.http.delete(`${this.url}/${id.id}`)
      .map(response => response.json())
      .catch(this.handleErrorResponse);
  }

  handleErrorResponse(error: Response) {
    switch (error.status) {
      case 404:
        return Observable.throw(new NotFoundError(error));

      case 400:
        return Observable.throw(new BadRequestError(error));

      default:
        return Observable.throw(new AppError(error));
    }
  }
}
