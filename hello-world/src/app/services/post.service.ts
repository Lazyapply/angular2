import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { DataService } from './DataService';

@Injectable()
export class PostService extends DataService {
  // private readonly accessUrl = 'http://jsonplaceholder.typicode.com/posts';

  constructor(http: Http) {
    super('http://jsonplaceholder.typicode.com/posts', http);
  }


}
