import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ng-switch',
  templateUrl: './ng-switch.component.html',
  styleUrls: ['./ng-switch.component.css']
})
export class NgSwitchComponent {
  viewMode = 'map';
  courses;
  courses2 = [
    { id: 1, name: 'course 1'},
    { id: 2, name: 'course 2'},
    { id: 3, name: 'course 3'},
  ];

  onAdd() {
    this.courses2.push({ id: 4, name: 'course 4'});
  }

  onChange(course) {
    // this.courses.splice(indexCourse, 1);
    course.name = 'UPDATED';
  }

  loadCourses() {
    this.courses = this.courses2;
  }

  trackCourse(index, course) {
    return course ? course.id : undefined;
  }

  constructor() { }

}
