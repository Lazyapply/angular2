import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../services/post.service';
import { AppError } from '../ErrorHandlers/AppError';

@Component({
  selector: 'app-posts-component',
  templateUrl: './posts-component.component.html',
  styleUrls: ['./posts-component.component.css']
})
export class PostsComponentComponent implements OnInit {
  posts: any[];

  constructor(private postService: PostService) {}

  ngOnInit() {
    this.postService.getAll()
      .subscribe(posts => this.posts = posts);
  }

  createPost(input: HTMLInputElement) {
    const postData: any = { title: input.value };
    this.posts.splice(0, 0, postData);

    input.value = '';

    this.postService.create(postData)
      .subscribe(newPost => {
          postData['id'] = newPost.id;
          console.log(newPost);
        }, (error: AppError) => {
        this.posts.splice(0, 1);

        throw error;
      });
  }

  updatePost(post) {
    this.postService.update(post)
      .subscribe(updatedPost => console.log(updatedPost));
  }

  deletePost(post) {
    const index = this.posts.indexOf(post);
    this.posts.splice(index, 1);

    this.postService.delete(post)
      .subscribe(null, (error: AppError) => {
        this.posts.splice(index, 0, post);

        throw error;
      });
  }
}
