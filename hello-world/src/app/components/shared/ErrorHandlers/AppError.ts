export class AppError {
  constructor(public originalError?: any) {
    if (originalError) {
      console.log(originalError.statusText);
    }
  }
}
