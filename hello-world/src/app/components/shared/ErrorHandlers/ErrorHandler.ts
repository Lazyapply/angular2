

import { ErrorHandler } from '@angular/core';
import { AppError } from './AppError';
import { NotFoundError } from './NotFoundError';
import { BadRequestError } from './BadRequestError';

export class AppErrorHandler implements ErrorHandler {
  handleError(error: AppError) {
    let message = '';

    switch (error.constructor) {
      case NotFoundError:
        message = 'This post does not exists.';
        break;

      case BadRequestError:
        message = 'This post has already been deleted.';
        break;

      default:
        message = 'Unexpected error occurred.';
    }

    alert(message);
  }
}
