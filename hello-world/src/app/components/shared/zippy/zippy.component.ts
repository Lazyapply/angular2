import { Component, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'bs-zippy',
  templateUrl: './zippy.component.html',
  styleUrls: ['./zippy.component.css']
})
export class ZippyComponent {
  @Input('title') title: string;
  isExpanded: boolean = false;

  constructor(private el: ElementRef) {  }

  onClick() {
    this.isExpanded = !this.isExpanded;
  }


}
