import { Component, Input } from '@angular/core';

@Component({
  selector: 'like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent {
  @Input() likesCount: number;
  @Input() isSelected: boolean;

  onClick() {
    this.likesCount += this.isSelected ? -1 : 1;
    this.isSelected = !this.isSelected;
  }
}
