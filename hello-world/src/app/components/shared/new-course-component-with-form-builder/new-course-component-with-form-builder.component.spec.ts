import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCourseComponentWithFormBuilderComponent } from './new-course-component-with-form-builder.component';

describe('NewCourseComponentWithFormBuilderComponent', () => {
  let component: NewCourseComponentWithFormBuilderComponent;
  let fixture: ComponentFixture<NewCourseComponentWithFormBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCourseComponentWithFormBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCourseComponentWithFormBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
