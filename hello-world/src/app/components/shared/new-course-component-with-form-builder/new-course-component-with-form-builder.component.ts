import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-course-component-with-form-builder',
  templateUrl: './new-course-component-with-form-builder.component.html',
  styleUrls: ['./new-course-component-with-form-builder.component.css']
})
export class NewCourseComponentWithFormBuilderComponent {

  form;

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      name: ['Enter your name', Validators.required],
      contact: fb.group({
        email: [],
        phone: [],
      }),
      topics: fb.array([]),
    });
  }
}
