import { Component } from '@angular/core';
import { FavoriteChangedEventArgs } from "./components/shared/favorites/favorites.component";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  courses = [1, 2, 3];

  post = {
    title: 'title',
    isFavorite: true,
  };

  like = {
    likesCount: 10,
    isSelected: false
  };

  onFavoriteChanged(eventArgs: FavoriteChangedEventArgs) {
    console.log('Favorite changed: ', eventArgs.newValue);
  }
}
